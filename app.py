from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy, sqlalchemy
import requests
import datetime
import pytz
import json
from operator import itemgetter

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////root/lboard/data/report.db'
db = SQLAlchemy(app)

app.config.update(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 465,
    MAIL_USE_SSL = True,
    MAIL_USE_TLS = False,
    MAIL_USERNAME = 'zack.gray@levelsolar.com',
    MAIL_PASSWORD = 'levelsolar'
    )

class KPI(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    ambassador = db.Column(db.String)
    amb_id = db.Column(db.String)
    county = db.Column(db.String)
    pod = db.Column(db.String)
    sets = db.Column(db.Integer)
    sits = db.Column(db.Integer)
    sales = db.Column(db.Integer)
    potential_sits = db.Column(db.Integer)
    sit_rate = db.Column(db.String)

    def __init__(self, date, ambassador, amb_id, county, pod, sets, sits, sales, potential_sits, sit_rate):
        self.date = date
        self.ambassador = ambassador
        self.amb_id = amb_id
        self.county = county
        self.pod = pod
        self.sets = sets
        self.sits = sits
        self.sales = sales
        self.potential_sits = potential_sits
        self.sit_rate = sit_rate

    def __repr__(self):
        return '%r - %r' % (self.ambassador, self.date)

class Metric(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    ambassador = db.Column(db.String)
    amb_id = db.Column(db.String)
    county = db.Column(db.String)
    pod = db.Column(db.String)
    doors = db.Column(db.Integer)
    prospects = db.Column(db.Integer)
    pitches = db.Column(db.Integer)
    appointments = db.Column(db.Integer)
    leads = db.Column(db.Integer)

    def __init__(self, date, ambassador, amb_id, county, pod, doors, prospects, pitches, appointments, leads):
        self.date = date
        self.ambassador = ambassador
        self.amb_id = amb_id
        self.county = county
        self.pod = pod
        self.doors = doors
        self.prospects = prospects
        self.pitches = pitches
        self.appointments = appointments
        self.leads = leads

    def __repr__(self):
        return '%r - %r' % (self.ambassador, self.date)

class Appointment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    ambassador = db.Column(db.String)
    amb_id = db.Column(db.String)
    assigned_to = db.Column(db.String)
    status = db.Column(db.String)
    closer = db.Column(db.String)
    county = db.Column(db.String)
    lead_id = db.Column(db.String)
    name = db.Column(db.String)
    address = db.Column(db.String)
    phone = db.Column(db.String)
    outcome = db.Column(db.String)
    csr_notes = db.Column(db.String)
    field_notes = db.Column(db.String)
  

    def __init__(self, date, ambassador, amb_id, assigned_to, status, closer, county, lead_id, name, address, phone, outcome, csr_notes, field_notes):
        self.date = date
        self.ambassador = ambassador
        self.amb_id = amb_id
        self.assigned_to = assigned_to
        self.status = status
        self.closer = closer
        self.county = county
        self.lead_id = lead_id
        self.name = name
        self.address = address
        self.phone = phone
        self.outcome = outcome
        self.csr_notes = csr_notes
        self.field_notes = field_notes

    def __repr__(self):
        return '%r - %r' % (self.name, self.date)

class Install(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    ambassador = db.Column(db.String)
    closer = db.Column(db.String)
    name = db.Column(db.String)
    address = db.Column(db.String)
    county = db.Column(db.String)

    def __init__(self, date, ambassador, closer, name, address, county):
        self.date = date
        self.ambassador = ambassador
        self.closer = closer
        self.name = name
        self.address = address
        self.county = county

    def __repr__(self):
        return '%r - %r' % (self.name, self.date)

def times():
    global yesterday_12am_est, today_12am_est, today_1159_est
    utc_zone = pytz.timezone('UTC')
    est_zone = pytz.timezone('US/Eastern')
    now_utc_naive = datetime.datetime.utcnow()
    now_utc_aware = utc_zone.localize(now_utc_naive)
    now_est_aware = now_utc_aware.astimezone(est_zone)
    today_1159_est = now_est_aware.replace(hour=23, minute=59, second=59)
    today_12am_est = now_est_aware.replace(hour=0, minute=0, second=0)
    one_day = datetime.timedelta(days=1)
    yesterday_12am_est = today_12am_est - one_day

@app.route('/')
def overview():
    data_file = open("/root/lboard/data/overview.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("overview.html", info=info)

@app.route('/nassau/yesterday')
def appts_yest_nassau():
    times()
    info = []
    appts = Appointment.query.filter(
                                 sqlalchemy.and_(Appointment.date.between(yesterday_12am_est, today_12am_est),
                                 sqlalchemy.or_(Appointment.county=="Nassau", Appointment.county=="nassau"))).all()
    for item in appts:
        appt = {}
        if len(item.ambassador.split()) > 1:
            appt["ambassador"] = item.ambassador.split()[1]
        else:
            appt["ambassador"] = "None"
        appt["status"] = item.status
        if len(item.assigned_to.split()) > 1:
            appt["assigned_to"] = item.assigned_to.split()[1]
        else:
            appt["assigned_to"] = None
        if len(item.closer.split()) > 1:
            appt["closer"] = item.closer.split()[1]
        else:
            appt["closer"] = None
        appt["outcome"] = item.outcome
        appt["csr_notes"] = item.csr_notes
        appt["field_notes"] = item.field_notes
        appt["name"] = item.name
        appt["address"] = item.address
        appt["phone"] = item.phone
        info.append(appt)
    info_sorted = sorted(info, key=itemgetter("ambassador"))
    return render_template("nassau_outcomes.html", info=info_sorted, market="Nassau")

@app.route('/suffolk/yesterday')
def appts_yest_suffolk():
    times()
    info = []
    appts = Appointment.query.filter(
                                 sqlalchemy.and_(Appointment.date.between(yesterday_12am_est, today_12am_est),
                                 sqlalchemy.or_(Appointment.county=="Suffolk", Appointment.county=="suffolk"))).all()
    for item in appts:
        appt = {}
        if len(item.ambassador.split()) > 1:
            appt["ambassador"] = item.ambassador.split()[1]
        else:
            appt["ambassador"] = "None"
        appt["status"] = item.status
        if len(item.assigned_to.split()) > 1:
            appt["assigned_to"] = item.assigned_to.split()[1]
        else:
            appt["assigned_to"] = None
        if len(item.closer.split()) > 1:
            appt["closer"] = item.closer.split()[1]
        else:
            appt["closer"] = None
        appt["outcome"] = item.outcome
        appt["csr_notes"] = item.csr_notes
        appt["field_notes"] = item.field_notes
        appt["name"] = item.name
        appt["address"] = item.address
        appt["phone"] = item.phone
        info.append(appt)
    info_sorted = sorted(info, key=itemgetter("ambassador"))
    return render_template("suffolk_outcomes.html", info=info_sorted)

@app.route('/suffolk/today')
def appts_today_suffolk():
    data_file = open("/root/lboard/data/scheduled_suffolk.json", "r")
    info = json.load(data_file)
    data_file.close()
    info_sorted = sorted(info, key=itemgetter("ambassador"))
    return render_template("suffolk_scheduled.html", info=info_sorted)

@app.route('/nassau/today')
def appts_today_nassau():
    data_file = open("/root/lboard/data/scheduled_nassau.json", "r")
    info = json.load(data_file)
    data_file.close()
    info_sorted = sorted(info, key=itemgetter("ambassador"))
    return render_template("nassau_scheduled.html", info=info_sorted)

@app.route('/nassau')
def nassau():
    data_file = open("/root/lboard/data/pods.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("nassau.html", info=info)

@app.route('/nassau/last_week')
def nassau_last_week():
    data_file = open("/root/lboard/data/podslw.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("nassau.html", info=info)

@app.route('/suffolk')
def suffolk():
    data_file = open("/root/lboard/data/pods.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("suffolk.html", info=info)

@app.route('/suffolk/last_week')
def suffolk_last_week():
    data_file = open("/root/lboard/data/podslw.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("suffolk.html", info=info)

@app.route('/suffolk/metrics')
def suffolk_metrics():
    data_file = open("/root/lboard/data/pods.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("suffolk_metrics.html", info=info)

@app.route('/suffolk/metrics/last_week')
def suffolk_metrics_last_week():
    data_file = open("/root/lboard/data/podslw.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("suffolk_metrics.html", info=info)

@app.route('/nassau/metrics')
def nassau_metrics():
    data_file = open("/root/lboard/data/pods.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("nassau_metrics.html", info=info)

@app.route('/nassau/metrics/last_week')
def nassau_metrics_last_week():
    data_file = open("/root/lboard/data/podslw.json", "r")
    info = json.load(data_file)
    data_file.close()
    return render_template("nassau_metrics.html", info=info)

#if __name__ == "__main__":
#    app.run(debug=True)

