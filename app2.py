from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
import requests
import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///C:\\Users\\Zack\\desktop\\dbase\\test.db'
db = SQLAlchemy(app)

class Test(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.String)
    ambassador = db.Column(db.String)

    def __init__(self, ambassador, date):
        pass

    def __repr__(self):
        return '%r - %r' % (self.ambassador, self.date)

def api_response(url, parameters):
	response_json = requests.get(url, params=parameters).json()
	return response_json

@app.route('/')
def dabo():
    return "1"

if __name__ == "__main__":
    app.run(debug=True)

"""
leads = "https://levelsolar.secure.force.com/api/services/apexrest/leads"
contacts = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
interactions = "https://levelsolar.secure.force.com/api/services/apexrest/interactions"
accounts = "https://levelsolar.secure.force.com/api/services/apexrest/accounts"
cases = "https://levelsolar.secure.force.com/api/services/apexrest/cases"

kunzler = "a027000000RHoIZ"

lead_list = api_response(leads, {"ambassador": kunzler})

for item in lead_list:
	lead = Lead(item["name"], "Gavin Kunzler")
	db.session.add(lead)
db.session.commit()
"""