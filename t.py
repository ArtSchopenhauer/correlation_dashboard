from flask.ext.sqlalchemy import SQLAlchemy, sqlalchemy
import datetime
import pytz
import dateutil.parser
import requests
import json
from app import db, KPI, Metric, Appointment, Install

utc_zone = pytz.timezone('UTC')
est_zone = pytz.timezone('US/Eastern')
now_utc_naive = datetime.datetime.utcnow()
now_utc_aware = utc_zone.localize(now_utc_naive)
now_est_aware = now_utc_aware.astimezone(est_zone)
today_12am_est = now_est_aware.replace(hour=0, minute=0, second=0)
today_12am_est_in_utc = today_12am_est.astimezone(utc_zone)
today_12am_est_in_utc_naive = today_12am_est_in_utc.replace(tzinfo=None)
today_12am_iso = today_12am_est_in_utc_naive.isoformat()
one_day = datetime.timedelta(days=1)
yesterday_12am_est = today_12am_est - one_day
yesterday_12am_est_in_utc = yesterday_12am_est.astimezone(utc_zone)
yesterday_12am_est_in_utc_naive = yesterday_12am_est_in_utc.replace(tzinfo=None)
yesterday_12am_iso = yesterday_12am_est_in_utc_naive.isoformat()
tomorrow_12am_est = today_12am_est + one_day
tomorrow_12am_est_in_utc = tomorrow_12am_est.astimezone(utc_zone)
tomorrow_12am_est_in_utc_naive = tomorrow_12am_est_in_utc.replace(tzinfo=None)
tomorrow_12am_iso = tomorrow_12am_est_in_utc_naive.isoformat()


appts = Appointment.query.filter(sqlalchemy.and_(Appointment.date.between(yesterday_12am_est, today_12am_est),
	                             sqlalchemy.or_(Appointment.county=="Nassau", Appointment.county=="nassau"))).all()
print len(appts)