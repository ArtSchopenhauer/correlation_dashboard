from flask.ext.sqlalchemy import SQLAlchemy, sqlalchemy
import datetime
import pytz
import time
import dateutil.parser
import requests
import json
from flask_mail import Mail, Message
from app import app, db, KPI, Metric, Appointment, Install

leads = "https://levelsolar.secure.force.com/api/services/apexrest/leads"
contacts = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
interactions = "https://levelsolar.secure.force.com/api/services/apexrest/interactions"
accounts = "https://levelsolar.secure.force.com/api/services/apexrest/accounts"
cases = "https://levelsolar.secure.force.com/api/services/apexrest/cases"
metrics = "https://levelsolar.secure.force.com/api/services/apexrest/metrics"

pod_names = [{"name": "n1", "county": "Nassau", "lead": "Ryan Samida"},
			 {"name": "n2", "county": "Nassau", "lead": "Casey O'Brien"},
			 {"name": "s1", "county": "Suffolk", "lead": "Nestor Colon"},
			 {"name": "s2", "county": "Suffolk", "lead": "Francis D'Erasmo"},
			 {"name": "s3", "county": "Suffolk", "lead": "Mike Desiderio"}]

pods = {"n1": [{"name": "Ryan Samida (L)", "id": "a027000000RvoO8"},
			   {"name": "Brenda Larsen", "id": "a027000000SKNZq"},
			   {"name": "Caitlyn Weiss", "id": "a027000000SeQfE"},
			   {"name": "Lonnie Edwards", "id": "a027000000Qqn7u"},
			   {"name": "Andrew Drewchin", "id": "a027000000SeRyO"},
			   {"name": "Alex Desinor", "id": "a023900000T0Lu0"}], 
		"n2": [{"name": "Casey O'Brien (L)", "id": "a027000000Q86c5"},
			   {"name": "Andrew Field", "id": "a027000000Rlv53"},
			   {"name": "Jeremy Weissman", "id": "a027000000PXjka"},
			   {"name": "Andrew Malca", "id": "a027000000SHtTj"},
			   {"name": "Dorothy Pitti", "id": "a027000000SgtbB"},
			   {"name": "Carlo Echeverri", "id": "a023900000TMyEw"}],
		"s1": [{"name": "Nestor Colon (L)", "id": "a027000000PxIOQ"},
			   {"name": "Carlos Vega", "id": "a027000000R3JB2"},
			   {"name": "Robert Dees", "id": "a027000000RvoZ7"}],
		"s2": [{"name": "Francis D'Erasmo (L)", "id": "a027000000Px6vS"},
			   {"name": "Mike Russo", "id": "a027000000Pue1Z"},
			   {"name": "Juliana Eckel", "id": "a027000000SbnHW"}],
		"s3": [{"name": "Michael Desiderio (L)", "id": "a027000000QsUeE"},
			   {"name": "Raymond Armstrong", "id": "a027000000SKN4R"},
			   {"name": "Victor Borisov", "id": "a027000000RyOFN"}]}

utc_zone = pytz.timezone('UTC')
est_zone = pytz.timezone('US/Eastern')
now_utc_naive = datetime.datetime.utcnow()
now_utc_aware = utc_zone.localize(now_utc_naive)
now_est_aware = now_utc_aware.astimezone(est_zone)
today_12am_est = now_est_aware.replace(hour=0, minute=0, second=0)
today_12am_est_in_utc = today_12am_est.astimezone(utc_zone)
today_12am_est_in_utc_naive = today_12am_est_in_utc.replace(tzinfo=None)
today_12am_iso = today_12am_est_in_utc_naive.isoformat()
one_day = datetime.timedelta(days=1)
yesterday_12am_est = today_12am_est - one_day
yesterday_12am_est_in_utc = yesterday_12am_est.astimezone(utc_zone)
yesterday_12am_est_in_utc_naive = yesterday_12am_est_in_utc.replace(tzinfo=None)
yesterday_12am_iso = yesterday_12am_est_in_utc_naive.isoformat()
yesterday_10am_est = yesterday_12am_est.replace(hour=10, minute=0, second=0)
tomorrow_12am_est = today_12am_est + one_day
tomorrow_12am_est_in_utc = tomorrow_12am_est.astimezone(utc_zone)
tomorrow_12am_est_in_utc_naive = tomorrow_12am_est_in_utc.replace(tzinfo=None)
tomorrow_12am_iso = tomorrow_12am_est_in_utc_naive.isoformat()
last_month_12am_est = today_12am_est - (one_day * 30)
last_month_12am_est_in_utc = last_month_12am_est.astimezone(utc_zone)
last_month_12am_est_in_utc_naive = last_month_12am_est_in_utc.replace(tzinfo=None)
last_month_12am_iso = last_month_12am_est_in_utc_naive.isoformat()

# takes aware EST datetime and returns Salesforce API parameter (i.e. UTC isoformat)
def make_iso(datetime_est):
	datetime_est_in_utc = datetime_est.astimezone(utc_zone)
	datetime_est_in_utc_naive = datetime_est_in_utc.replace(tzinfo=None)
	datetime_iso = datetime_est_in_utc_naive.isoformat()
	return datetime_iso

def send_error():
	mail = Mail(app)
	msg = Message('Error: API Call Failed 10 Times', sender='Zack Gray', recipients=['zspencergray@gmail.com'])
	mail.send(msg)

# returns Monday 12am EST as naive UTC datetime
def week_start():
	one_day = datetime.timedelta(days=1)
	today_int = now_est_aware.weekday()
	this_time_est_monday = now_est_aware - (one_day * today_int)
	offset = (now_utc_aware.hour - now_est_aware.hour)
	if offset < 0:
		offset = (now_utc_aware.hour + 24) - now_est_aware.hour
	monday_12am_est_aware = this_time_est_monday.replace(hour=offset, minute=0, second=0, microsecond=0)
	monday_12am_est_naive = monday_12am_est_aware.replace(tzinfo=None)
	return monday_12am_est_naive

week_start_est = week_start()
last_week_start_est = week_start_est - (one_day * 7)

def api_response(url, parameters, try_num):
	if try_num <= 9:
		r = requests.get(url, params=parameters)
		if r.status_code == 200:
			return r.json()
		else:
			try_num = try_num + 1
			time.sleep(1)
			api_response(url, parameters, try_num)
	else:
		print "Tried and failed 10 times."
		with app.app_context():
			send_error()

# fetches appointment outcomes from SFDC and returns list of record entries for Appointment table
def get_outcomes():
	appointment_entries = []
	appt_list = api_response(interactions, {"subject": "Closer Appointment",
											  "scheduled_date__gt": yesterday_12am_iso,
											  "scheduled_date__lt": today_12am_iso}, 0)
	for appt in appt_list:
		date = dateutil.parser.parse(appt["scheduled_date"]).astimezone(est_zone)
		if appt["assigned_to"]:
			assigned_to = appt["assigned_to"]["name"]
		else:
			assigned_to = "None"
		if appt["canceled"] == True:
			status = "Postponed"
		elif appt["confirmed"] == True:
			status = "Confirmed"
		else:
			status = "Unconfirmed"
		if appt["comments"]:
			csr_notes = appt["comments"]
		else:
			csr_notes = "None"
		if appt["lead"]:
			lead_id = appt["lead"]["id"]
			if appt["lead"]["ambassador"]:
				ambassador = appt["lead"]["ambassador"]["name"]
				amb_id = appt["lead"]["ambassador"]["id"][0:15]
			else:
				ambassador = "None"
				amb_id = "None"
			if appt["lead"]["closer"]:
				closer = appt["lead"]["closer"]["name"]
			else:
				closer = "None"
			if appt["lead"]["county"]:
				county = appt["lead"]["county"]
			else:
				county = "None"
			if appt["lead"]["name"]:
				name = appt["lead"]["name"]
			else:
				name = "None"
			if appt["lead"]["street_address"]:
				street_address = appt["lead"]["street_address"]
			else:
				street_address = ""
			if appt["lead"]["city"]:
				city = appt["lead"]["city"]
			else:
				city = ""
			address = street_address + ", " + city
			if appt["lead"]["mobilephone"]:	
				phone = appt["lead"]["mobilephone"]
			elif appt["lead"]["phone"]:
				phone = appt["lead"]["phone"]
			else:
				phone = "None"
			if appt["lead"]["notes"]:
				field_notes_raw = appt["lead"]["notes"]
				if "null" in field_notes_raw:
					field_notes = field_notes_raw.replace("null", "Closer: ")
				else:
					field_notes = "Setter: " + field_notes_raw
			else:
				field_notes = "None"
			lead_url = leads + "/" + lead_id
			lead = api_response(lead_url, "", 0)[0]
			if lead["status"]["sale"] == True:
				outcome = "Sale"
			elif lead["status"]["appointment_sit"] == True:
				outcome = "Sat, No Sale"
			elif closer == "None":
				outcome = "No Outcome"
			else:
				outcome = "Did Not Sit"
		else:
			ambassador, amb_id, closer, county, lead_id, name, address, phone, field_notes, outcome = "None"
		appointment_entry = Appointment(date, ambassador, amb_id, assigned_to, status, closer, county,
										lead_id, name, address, phone, outcome, csr_notes, field_notes)
		appointment_entries.append(appointment_entry)
	return appointment_entries

# stores today's appointments api_response
def fill_scheduled():
	info_suffolk = []
	info_nassau = []
	appt_list = api_response(interactions, {"subject": "Closer Appointment",
											  "scheduled_date__gt": today_12am_iso,
											  "scheduled_date__lt": tomorrow_12am_iso}, 0)
	for appt in appt_list:
		item = {}
		date = dateutil.parser.parse(appt["scheduled_date"]).astimezone(est_zone)
		if date.minute == 0:
			item["time"] = str(date.hour) + ":" + "00"
		else:
			item["time"] = str(date.hour) + ":" + str(date.minute)
		if appt["assigned_to"]:
			if len(appt["assigned_to"]["name"]) > 1:
				item["assigned_to"] = appt["assigned_to"]["name"].split()[1]
			else:
				item["assigned_to"] = appt["assigned_to"]["name"]
		else:
			item["assigned_to"] = "None"
		if appt["canceled"] == True:
			item["status"] = "Postponed"
		elif appt["confirmed"] == True:
			item["status"] = "Confirmed"
		else:
			item["status"] = "Unconfirmed"
		if appt["comments"]:
			item["csr_notes"] = appt["comments"]
		else:
			item["csr_notes"] = "None"
		if appt["lead"]:
			lead_id = appt["lead"]["id"]
			if appt["lead"]["ambassador"]:
				if len(appt["lead"]["ambassador"]["name"]) > 1:
					item["ambassador"] = appt["lead"]["ambassador"]["name"].split()[1]
				else:
					item["ambassador"] = appt["lead"]["ambassador"]["name"]
			else:
				item["ambassador"] = "None"
			if appt["lead"]["county"]:
				item["county"] = appt["lead"]["county"]
			else:
				item["county"] = "None"
			if appt["lead"]["name"]:
				item["name"] = appt["lead"]["name"]
			else:
				item["name"] = "None"
			if appt["lead"]["street_address"]:
				street_address = appt["lead"]["street_address"]
			else:
				street_address = ""
			if appt["lead"]["city"]:
				city = appt["lead"]["city"]
			else:
				city = ""
			item["address"] = street_address + ", " + city
			if appt["lead"]["mobilephone"]:	
				item["phone"] = appt["lead"]["mobilephone"]
			elif appt["lead"]["phone"]:
				item["phone"] = appt["lead"]["phone"]
			else:
				item["phone"] = "None"
			if appt["lead"]["notes"]:
				field_notes_raw = appt["lead"]["notes"]
				if "null" in field_notes_raw:
					item["field_notes"] = field_notes_raw.replace("null", "Closer: ")
				else:
					item["field_notes"] = "Setter: " + field_notes_raw
			else:
				item["field_notes"] = "None"
		else:
			item["ambassador"], item["county"], item["name"], item["address"],
			item["phone"], item["field_notes"] = "None"
		if item["county"] == "Suffolk" or item["county"] == "suffolk":
			info_suffolk.append(item)
		elif item["county"] == "Nassau" or item["county"] == "nassau":
			info_nassau.append(item)
	suffolk_file = open("/root/lboard/data/scheduled_suffolk.json", "w")
	json.dump(info_suffolk, suffolk_file)
	suffolk_file.close()
	nassau_file = open("/root/lboard/data/scheduled_nassau.json", "w")
	json.dump(info_nassau, nassau_file)
	nassau_file.close()

def get_sets_period(ambassador_id, start, end):
	leads = []
	params = {"subject": "Closer Appointment", "ambassador": ambassador_id,
			  "createddate__gt": start, "createddate__lt": end}
	r = api_response(interactions, params, 0)
	for item in r:
		if item["lead"]:
			leads.append(item["lead"]["street_address"])
	return len(list(set(leads)))

def amb_metric_period(ambassador_id, metric, start, end):
	lead_list = []
	param = {"subject": "Closer Appointment", "ambassador": ambassador_id,
	         "scheduled_date__gt": start, "scheduled_date__lt": end}
	r = api_response(interactions, param, 0)
	for item in r:
		if item["lead"]:
			lead_id = item["lead"]["id"]
			url = leads + "/" + lead_id
			lead = api_response(url, "", 0)[0]
			if lead["status"][metric] == True:
				lead_list.append(lead_id)
	return len(list(set(lead_list)))

# fetches data from SFDC and returns list of entries for KPI table
def get_kpis():
	kpi_entries = []
	date = yesterday_10am_est
	for item in pod_names:
		for amb in pods[item["name"]]:
			ambassador = amb["name"]
			amb_id = amb["id"]
			county = item["county"]
			pod = item["name"]
			sets = get_sets_period(amb_id, yesterday_12am_iso, today_12am_iso)
			sits = amb_metric_period(amb_id, "appointment_sit", yesterday_12am_iso, today_12am_iso)
			sales = amb_metric_period(amb_id, "sale", yesterday_12am_iso, today_12am_iso)
			potential_sits = 0
			outcomes_yest = Appointment.query.filter(
		                         sqlalchemy.and_(Appointment.date.between(yesterday_12am_est, today_12am_est),
		                         Appointment.amb_id==amb_id)).all()
			for outcome in outcomes_yest:
				if (len(outcome.assigned_to) > 4) or (len(outcome.assigned_to) <= 4 and outcome.status != "Postponed"):
					potential_sits = potential_sits + 1
			if potential_sits == 0:
				sit_rate = "0%"
			else:
				sit_rate = "{0:.0f}%".format((float(sits)/float(potential_sits))*100)
			kpi_entry = KPI(date, ambassador, amb_id, county, pod, sets, sits, sales, potential_sits, sit_rate)
			kpi_entries.append(kpi_entry)
	return kpi_entries

def get_metrics():
	metrics_entries = []
	submitted_ids = []
	date = yesterday_10am_est
	metrics_list = api_response(metrics, {"createddate__gt": yesterday_12am_iso, "createddate__lt": today_12am_iso}, 0)
	for metric in metrics_list:
		submitted_ids.append(metric["ambassador"]["id"][0:15])
	for item in pod_names:
		for amb in pods[item["name"]]:
			county = item["county"]
			pod = item["name"]
			ambassador = amb["name"]
			amb_id = amb["id"]
			if amb_id in submitted_ids:
				for metric in metrics_list:
					if metric["ambassador"]["id"][0:15] == amb_id:
						doors = metric["doors"]
						prospects = metric["prospects"]
						pitches = metric["pitches"]
						appointments = metric["appointments"]
						contacts = metric["contacts"]
			else:
				doors = 0
				prospects = 0
				pitches = 0
				appointments = 0
				contacts = 0
			metrics_entry = Metric(date, ambassador, amb_id, county, pod, doors, prospects, pitches, appointments, contacts)
			metrics_entries.append(metrics_entry)
	return metrics_entries
	
def fill_overview():
	info = {"nassau": {"potentialsitsyd": 0, "potentialsitstw": 0, "setsyd": 0, "sitsyd": 0, "salesyd": 0, "sitrateyd": "", "setstw": 0, "sitstw": 0, "salestw": 0, "sitratetw": ""},
	        "suffolk": {"potentialsitsyd": 0, "potentialsitstw": 0, "setsyd": 0, "sitsyd": 0, "salesyd": 0, "sitrateyd": "", "setstw": 0, "sitstw": 0, "salestw": 0, "sitratetw": ""},
	        "total": {"potentialsitsyd": 0, "potentialsitstw": 0, "setsyd": 0, "sitsyd": 0, "salesyd": 0, "sitrateyd": "", "setstw": 0, "sitstw": 0, "salestw": 0, "sitratetw": ""}}
	potential_sits_suffolk_yd = 0
	potential_sits_nassau_yd = 0
	potential_sits_suffolk_tw = 0
	potential_sits_nassau_tw = 0
	kpitw_suffolk = KPI.query.filter(
		                         sqlalchemy.and_(KPI.date.between(week_start_est, today_12am_est),
                                 sqlalchemy.or_(KPI.county=="Suffolk", KPI.county=="suffolk"))).all()
	kpitw_nassau = KPI.query.filter(
								 sqlalchemy.and_(KPI.date.between(week_start_est, today_12am_est),
                                 sqlalchemy.or_(KPI.county=="Nassau", KPI.county=="nassau"))).all()
	kpiyd_suffolk = KPI.query.filter(
								 sqlalchemy.and_(KPI.date.between(yesterday_12am_est, today_12am_est),
                                 sqlalchemy.or_(KPI.county=="Suffolk", KPI.county=="suffolk"))).all()
	kpiyd_nassau = KPI.query.filter(
								 sqlalchemy.and_(KPI.date.between(yesterday_12am_est, today_12am_est),
                                 sqlalchemy.or_(KPI.county=="Nassau", KPI.county=="nassau"))).all()
	for item in kpitw_suffolk:
		info["suffolk"]["setstw"] = info["suffolk"]["setstw"] + item.sets
		info["suffolk"]["sitstw"] = info["suffolk"]["sitstw"] + item.sits
		info["suffolk"]["salestw"] = info["suffolk"]["salestw"] + item.sales
		potential_sits_suffolk_tw = potential_sits_suffolk_tw + item.potential_sits
	for item in kpitw_nassau:
		info["nassau"]["setstw"] = info["nassau"]["setstw"] + item.sets
		info["nassau"]["sitstw"] = info["nassau"]["sitstw"] + item.sits
		info["nassau"]["salestw"] = info["nassau"]["salestw"] + item.sales
		potential_sits_nassau_tw = potential_sits_nassau_tw + item.potential_sits
	for item in kpiyd_suffolk:
		info["suffolk"]["setsyd"] = info["suffolk"]["setsyd"] + item.sets
		info["suffolk"]["sitsyd"] = info["suffolk"]["sitsyd"] + item.sits
		info["suffolk"]["salesyd"] = info["suffolk"]["salesyd"] + item.sales
		potential_sits_suffolk_yd = potential_sits_suffolk_yd + item.potential_sits
	for item in kpiyd_nassau:
		info["nassau"]["setsyd"] = info["nassau"]["setsyd"] + item.sets
		info["nassau"]["sitsyd"] = info["nassau"]["sitsyd"] + item.sits
		info["nassau"]["salesyd"] = info["nassau"]["salesyd"] + item.sales
		potential_sits_nassau_yd = potential_sits_nassau_yd + item.potential_sits
	info["nassau"]["sitrateyd"] = "{0:.0f}%".format((float(info["nassau"]["sitsyd"])/float(potential_sits_nassau_yd))*100)
	info["suffolk"]["sitrateyd"] = "{0:.0f}%".format((float(info["suffolk"]["sitsyd"])/float(potential_sits_suffolk_yd))*100)
	info["nassau"]["sitratetw"] = "{0:.0f}%".format((float(info["nassau"]["sitstw"])/float(potential_sits_nassau_tw))*100)
	info["suffolk"]["sitratetw"] = "{0:.0f}%".format((float(info["suffolk"]["sitstw"])/float(potential_sits_suffolk_tw))*100)
	info["nassau"]["potentialsitsyd"] = potential_sits_nassau_yd
	info["suffolk"]["potentialsitsyd"] = potential_sits_suffolk_yd
	info["nassau"]["potentialsitstw"] = potential_sits_nassau_tw
	info["suffolk"]["potentialsitstw"] = potential_sits_suffolk_tw
	info["total"]["potentialsitsyd"] = potential_sits_nassau_yd + potential_sits_suffolk_yd
	info["total"]["potentialsitstw"] = potential_sits_nassau_tw + potential_sits_suffolk_tw
	info["total"]["setsyd"] = info["suffolk"]["setsyd"] + info["nassau"]["setsyd"]
	info["total"]["sitsyd"] = info["suffolk"]["sitsyd"] + info["nassau"]["sitsyd"]
	info["total"]["salesyd"] = info["suffolk"]["salesyd"] + info["nassau"]["salesyd"]
	info["total"]["setstw"] = info["suffolk"]["setstw"] + info["nassau"]["setstw"]
	info["total"]["sitstw"] = info["suffolk"]["sitstw"] + info["nassau"]["sitstw"]
	info["total"]["salestw"] = info["suffolk"]["salestw"] + info["nassau"]["salestw"]
	info["total"]["sitrateyd"] = "{0:.0f}%".format((float((info["nassau"]["sitsyd"] + info["suffolk"]["sitsyd"]))/float((potential_sits_nassau_yd + potential_sits_suffolk_yd)))*100)
	info["total"]["sitratetw"] = "{0:.0f}%".format((float((info["nassau"]["sitstw"] + info["suffolk"]["sitstw"]))/float((potential_sits_nassau_tw + potential_sits_suffolk_tw)))*100)
	data_file = open("/root/lboard/data/overview.json", "w")
	json.dump(info, data_file)
	data_file.close()

def fill_pods():
	info = {}
	for item in pod_names:
		pod_name = item["name"]
		info[pod_name] = {"totals": {"sets": 0, "sits": 0, "sales": 0, "potential_sits": 0, "sit_rate": 0, 
		                  "shifts": 0, "doors": 0, "prospects": 0, "pitches": 0, "appointments": 0, "contacts": 0},
						  "ambs": []}
		kpi_data = KPI.query.filter(
			                 sqlalchemy.and_(KPI.date.between(yesterday_12am_est, today_12am_est),
            	             KPI.pod==item["name"])).all()
		metrics_data = Metric.query.filter(
			                 sqlalchemy.and_(Metric.date.between(yesterday_12am_est, today_12am_est),
            	             Metric.pod==item["name"])).all()
		for kpi_record in kpi_data:
			amb_kpi = {}
			amb_kpi["ambassador"] = kpi_record.ambassador
			amb_kpi["sets"] = kpi_record.sets
			amb_kpi["sits"] = kpi_record.sits
			amb_kpi["sales"] = kpi_record.sales
			amb_kpi["potential_sits"] = kpi_record.potential_sits
			if kpi_record.potential_sits == 0:
				amb_kpi["sit_rate"] = "0%"
			else:
				amb_kpi["sit_rate"] = "{0:.0f}%".format((float(kpi_record.sits)/float(kpi_record.potential_sits))*100)
			for metric_record in metrics_data:
				if metric_record.amb_id == kpi_record.amb_id:
					if metric_record.doors:
						amb_kpi["doors"] = metric_record.doors
					else:
						amb_kpi["doors"] = 0
					if metric_record.prospects:
						amb_kpi["prospects"] = metric_record.prospects
					else:
						amb_kpi["prospects"] = 0
					if metric_record.pitches:
						amb_kpi["pitches"] = metric_record.pitches
					else:
						amb_kpi["pitches"] = 0
					if metric_record.appointments:
						amb_kpi["appointments"] = metric_record.appointments
					else:
						amb_kpi["appointments"] = 0
					if metric_record.leads:
						amb_kpi["contacts"] = metric_record.leads
					else:
						amb_kpi["contacts"] = 0
					if amb_kpi["doors"] > 0:
						amb_kpi["shifts"] = 1
					else:
						amb_kpi["shifts"] = 0
			info[pod_name]["totals"]["sets"] = info[pod_name]["totals"]["sets"] + amb_kpi["sets"]
			info[pod_name]["totals"]["sits"] = info[pod_name]["totals"]["sits"] + amb_kpi["sits"]
			info[pod_name]["totals"]["sales"] = info[pod_name]["totals"]["sales"] + amb_kpi["sales"]
			info[pod_name]["totals"]["potential_sits"] = info[pod_name]["totals"]["potential_sits"] + amb_kpi["potential_sits"]
			info[pod_name]["totals"]["doors"] = info[pod_name]["totals"]["doors"] + amb_kpi["doors"]
			info[pod_name]["totals"]["prospects"] = info[pod_name]["totals"]["prospects"] + amb_kpi["prospects"]
			info[pod_name]["totals"]["pitches"] = info[pod_name]["totals"]["pitches"] + amb_kpi["pitches"]
			info[pod_name]["totals"]["appointments"] = info[pod_name]["totals"]["appointments"] + amb_kpi["appointments"]
			info[pod_name]["totals"]["contacts"] = info[pod_name]["totals"]["contacts"] + amb_kpi["contacts"]
			info[pod_name]["totals"]["shifts"] = info[pod_name]["totals"]["shifts"] + amb_kpi["shifts"]
			info[pod_name]["ambs"].append(amb_kpi)
		if info[pod_name]["totals"]["potential_sits"] == 0:
			info[pod_name]["totals"]["sit_rate"] = "0%"
		else:
			info[pod_name]["totals"]["sit_rate"] = "{0:.0f}%".format((float(info[pod_name]["totals"]["sits"])/float(info[pod_name]["totals"]["potential_sits"]))*100)
	data_file = open("/root/lboard/data/pods.json", "w")
	json.dump(info, data_file)
	data_file.close()

def fill_pods_lw():
	info = {}
	for item in pod_names:
		pod_name = item["name"]
		info[pod_name] = {"totals": {"sets": 0, "sits": 0, "sales": 0, "potential_sits": 0, "sit_rate": 0, 
		                  "shifts": 0, "doors": 0, "prospects": 0, "pitches": 0, "appointments": 0, "contacts": 0},
						  "ambs": []}
		for amb in pods[pod_name]:
			ambassador = amb["name"]
			amb_id = amb["id"]
			amb_kpi = {"ambassador": ambassador, "sets": 0, "sits": 0, "sales": 0, "potential_sits": 0, "sit_rate": "",
						"shifts": 0, "doors": 0, "prospects": 0, "pitches": 0, "contacts": 0}
			kpi_data = KPI.query.filter(
			                 sqlalchemy.and_(KPI.date.between(last_week_start_est, week_start_est),
            	             KPI.amb_id==amb_id)).all()
			metrics_data = Metric.query.filter(
			                 sqlalchemy.and_(Metric.date.between(last_week_start_est, week_start_est),
            	             Metric.amb_id==amb_id)).all()
			for kpi_record in kpi_data:
				amb_kpi["sets"] = amb_kpi["sets"] + kpi_record.sets
				amb_kpi["sits"] = amb_kpi["sits"] + kpi_record.sits
				amb_kpi["sales"] = amb_kpi["sales"] + kpi_record.sales
				amb_kpi["potential_sits"] = amb_kpi["potential_sits"] + kpi_record.potential_sits
			if amb_kpi["potential_sits"] == 0:
				amb_kpi["sit_rate"] = "0%"
			else:
				amb_kpi["sit_rate"] = "{0:.0f}%".format((float(amb_kpi["sits"])/float(amb_kpi["potential_sits"]))*100)
			for metric_record in metrics_data:
				if metric_record.doors:
					amb_kpi["doors"] = amb_kpi["doors"] + metric_record.doors
					if metric_record.doors > 0:
						amb_kpi["shifts"] = amb_kpi["shifts"] + 1
				if metric_record.prospects:
					amb_kpi["prospects"] = amb_kpi["prospects"] + metric_record.prospects
				if metric_record.pitches:
					amb_kpi["pitches"] = amb_kpi["pitches"] + metric_record.pitches
				if metric_record.leads:
					amb_kpi["contacts"] = amb_kpi["contacts"] + metric_record.leads
			info[pod_name]["ambs"].append(amb_kpi)
			info[pod_name]["totals"]["sets"] = info[pod_name]["totals"]["sets"] + amb_kpi["sets"]
			info[pod_name]["totals"]["sits"] = info[pod_name]["totals"]["sits"] + amb_kpi["sits"]
			info[pod_name]["totals"]["sales"] = info[pod_name]["totals"]["sales"] + amb_kpi["sales"]
			info[pod_name]["totals"]["potential_sits"] = info[pod_name]["totals"]["potential_sits"] + amb_kpi["potential_sits"]
			info[pod_name]["totals"]["doors"] = info[pod_name]["totals"]["doors"] + amb_kpi["doors"]
			info[pod_name]["totals"]["prospects"] = info[pod_name]["totals"]["prospects"] + amb_kpi["prospects"]
			info[pod_name]["totals"]["pitches"] = info[pod_name]["totals"]["pitches"] + amb_kpi["pitches"]
			info[pod_name]["totals"]["contacts"] = info[pod_name]["totals"]["contacts"] + amb_kpi["contacts"]
			info[pod_name]["totals"]["shifts"] = info[pod_name]["totals"]["shifts"] + amb_kpi["shifts"]
		if info[pod_name]["totals"]["potential_sits"] == 0:
			info[pod_name]["totals"]["sit_rate"] = "0%"
		else:
			info[pod_name]["totals"]["sit_rate"] = "{0:.0f}%".format((float(info[pod_name]["totals"]["sits"])/float(info[pod_name]["totals"]["potential_sits"]))*100)
	data_file = open("/root/lboard/data/podslw.json", "w")
	json.dump(info, data_file)
	data_file.close()

def fill_Appointment():
	outcomes = get_outcomes()
	for outcome in outcomes:
		db.session.add(outcome)
	db.session.commit()

def fill_KPI():
	kpi_entries = get_kpis()
	for kpi in kpi_entries:
		db.session.add(kpi)
	db.session.commit()

def fill_Metric():
	metrics_entries = get_metrics()
	for metric in metrics_entries:
		db.session.add(metric)
	db.session.commit()

fill_pods_lw()